package BurusovTest::Search;

#require 5.008_008;
# при подключении Mojolicious::Lite автоматически подключаются warnings и strict
# но лучше перебдеть ;)
use warnings;
use strict;
use utf8;
use feature ':5.10';
use Encode;
use Mojo::UserAgent;
use Mojo::Base 'Mojolicious::Controller';

sub parseLinks {
    my (@kWord) = @_;
    my @numbers;
    my @links;
    my @pages;
    my $word;
    my $i;
    my $link;
    my $tx;
    my $str;
    my $ua = Mojo::UserAgent->new(
        max_redirects   => 5,
        name  => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    );

    #Выбираем в массив все ссылки на главные страницы сайтов

    foreach $word (@kWord){
        $word =~ tr/' '/\+/;
        $tx = $ua->get("https://www.google.ru/search?q=".decode('utf8', $word));
        if (not defined($tx->res->dom('cite')->first)){
            return;
        }
        $link = $tx->res->dom('cite')->first->to_string; 
        $link =~ s/(<\/?cite>)|(\/?strong)|(https?:\/\/)?|(<\/?b>)?|(\/[\w]+)?|//g; #Вычистили тэги из найденных адресов
        push @links, $link;
    }

    undef($tx);

    #Парсим по-очереди все страницы из массива @links

    for ($i = 0; $i <= scalar $#kWord; $i=$i+1) {
        push @numbers, decode('utf8', $kWord[$i]);
        
        #Теперь надо взять первый элемент коллекции и распарсить его. Только и всего.
        $tx = $ua->get($links[$i]);
        if (defined($tx)) {
            for my $node ($tx->res->dom->descendant_nodes->each) {
                if ($node->type eq 'text') {
                    if ($node->content =~ /\+?\d?[\s|\-]\(?[\d{3}|\d{5}]\)?[\s|\-]?\d{1,4}[\-]\d{2,4}/)
                    {
                        #Чистим строки от лишних символов.
                        $str = $node->content;
                        $str =~ s/(\s)+|(№\s\d{2})|([А-Яа-я])+|(\.)+|([!:\/])|(\d{6}\s\d{3})//g;
                        $str =~ tr/','/' '/;
                    
                        if ($str =~ /^[^©]/)
                        {
                            push @numbers, $str;
                        }
                    }
                }
            }
        }
    }
    return \@numbers;
}

sub index {
    my $self=shift;
    return $self->render;
}

sub phones {
    my $self=shift;
    my $nums;
    my $str;
    my @result;
    open(my $fh, "<", "kWord.txt") or die "Can't open < input.txt: $!";
    while (! eof($fh) ) {
        defined( $_ = readline $fh ) or die "readline failed: $!";
        push @result, $_;
    }
    $nums = parseLinks(@result);
    if (not defined($nums)){
        return $self->render(template=>'fail');
    }
    return $self->render(num=>$nums);
}

1;
