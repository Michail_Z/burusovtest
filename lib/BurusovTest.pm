package BurusovTest;

use Mojo::Base 'Mojolicious';

sub startup {
    my $self = shift;

    #router
    my $r = $self->routes;
    $r->get('/')->to('search#index')->name('index');
    $r->post('/kWord')->to('search#phones')->name('phones');
}

1;
